ROS2開発の開発基盤となるテンプレートパッケージです．  
以下の利点があります．  

- 最小構成のテンプレート
- c++とpythonの共存化
- .vscodeのテンプレート

# 使い方
開発したいパッケージ名を決めて、以下の4箇所をそれに置き換える必要があります．

- CMakeLists.txt  
  ```cmake
  - project(ros2_base_pkg)
  + project(your_pkg)
  ``` 
- package.xml  
  ```xml
  - <name>ros2_base_pkg</name>
  + <name>your_pkg</name>
  ``` 
- ros2_base_pkg (folder name)  
  rename your_pkg
- ros2_base_pkg/import_module.py
  ```py      
    - from ros2_base_pkg.import_module import ....
    + from your_pkg.import_module import ....
  ``` 

以上です．


---

This is a template package that serves as a development platform for ROS2.  
It offers the following advantages  

- Minimum configuration template
- Coexistence of c++ and python
- .vscode templates

# How to use
You need to decide on the name of the package you want to develop and replace the following 4 places with it.

- CMakeLists.txt  
  ```cmake
  - project(ros2_base_pkg)
  + project(your_pkg)
  ```` package.xml 
- package.xml  
  ```xml
  - <name>ros2_base_pkg</name>
  + <name>your_pkg</name>
  ``` 
- ros2_base_pkg (folder name)  
  rename your_pkg
- ros2_base_pkg/import_module.py
  ```py      
  - from ros2_base_pkg.import_module import ....
  + from your_pkg.import_module import ....
  ```

That's all.